# These are just a bunch of commands I tend to forgot upset for, which can be nice to revist

* output from script while writing to a file, writes to debug.log and output

`./dump_vices.zsh  | tee -a debug.log`

* jq syntax over json []

` jq -C '.[] | .DeviceTypeName + ":" + .IPAddress + ":" + .CRIDFullNumber' `

* port forward ssh option. Fowards port 8529(arangodb), to localhost 80. This will open a reverse tunnel from the remote machine

` ssh -nNT -L 80:localhost:8529 workstation `

* multi processing in bash using xargs. N=15

` echo $cross_ids | jq ".[][][]" | grep If | sed 's/\"//g' | xargs -P 15  -i curl -X DELETE --user bomi:$passwd "http://cross.dev70.oss.globalconnect.intra/web/api/crud/link/{}" `

* snmpwalk pprint output.
` snmpwalk -On -v2c -c public 172.18.8.43 1.3.6.1.4.1.1991.1.1.3.29.2.1.1.19 -O qes` 



* Resources

cheat lvm
Expand vg:
pvresize /dev/sdc
lvresize -l +100%FREE /dev/vg01/data
resize2fs /dev/vg01/data

Create ...
pvcreate /dev/sdx
vgcreate vg00 /dev/sdx
lvcreate -n navn -L 5G vg00
mkfs.ext3 /dev/mapper/vg00-navn

Add extra physical volume ...
pvcreate /dev/sdx
vgextend vg0x /dev/sdx

#Exclusive Activation of a Volume Group in a Cluster
#Link --> https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/High_Availability_Add-On_Administration/s1-exclusiveactive-HAAA.html
1> vgs --noheadings -o vg_name
2> volume_list = [ "rhel_root", "rhel_home" ]
3> dracut -H -f /boot/initramfs-$(uname -r).img $(uname -r)
4> Reboot the node
5> uname -r to verify the correct initrd image

Vejledning til online partitions resize
https://docs.netic.dk/pages/viewpage.action?pageId=83730465
Her udvides part 2 og gives til /var
parted -- /dev/sda unit b print # part 2 skulle gerne v<C3><A6>re sidste partition p<C3><A5> disken !! Ellers stop
for device in ls /sys/class/scsi_device/; do echo 1 > /sys/class/scsi_device/$device/device/rescan; done
lsblk
parted -s -- /dev/sda rm 2 # Der vil komme meddelelse om fejl da partitionen er i brug
parted -s -- /dev/sda mkpart p 537919488B -0   # start af partitionen: 537919488B fundet med parted -- /dev/sda unit b print
parted -s -- /dev/sda set 2 lvm on
parted -- /dev/sda unit b print
partx -uv /dev/sda
partprobe
lsblk
pvresize /dev/sda2
lvresize -l+100%FREE /dev/mapper/rootdisk-var
resize2fs /dev/mapper/rootdisk-var




$ for host in ls /sys/class/scsi_host/; do echo "- - -" > /sys/class/scsi_host/$host/scan; done
$ for device in ls /sys/class/scsi_device/; do echo 1 > /sys/class/scsi_device/$device/device/rescan; done


# Cross tricks
 Reindex lucene index på testserver

 ''' java -jar deployables/netbuilder.jar --properties testserver/server.properties reindex'''

