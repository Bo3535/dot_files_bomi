# set modifier
set $super Mod4
set $alt Mod1

# set font
font pango: Noto Sans 8

# Use Mouse+$super to drag floating windows to their wanted position
floating_modifier $super

#autostart
exec --no-startup-id random_wallpaper
exec --no-startup-id xsettingsd &
exec --no-startup-id compton -b
exec --no-startup-id redshift-gtk -P -O 5500
exec --no-startup-id nm-applet
exec --no-startup-id /home/bo/dns_fix.sh


# use keybind instead
# start a terminal
bindsym $super+Return exec i3-sensible-terminal

# start dmenu (a program launcher)
bindsym $super+Shift+d exec i3-dmenu-desktop --dmenu="dmenu -i -fn 'Noto Sans:size=8'"
bindsym $super+d exec rofi -lines 12 -padding 18 -width 60 -location 0 -show drun -sidebar-mode -columns 3 -font 'Noto Sans 8'

# common apps keybinds
bindsym Print exec scrot 'Cheese_%a-%d%b%y_%H.%M.png' -e 'viewnior ~/$f'
bindsym $super+l exec lock_i3

# Dim lights for redshift
# Day light
bindsym F7 exec redshift-gtk -P -O 5500
# night light
bindsym F8 exec redshift-gtk -P -O 3700

#change volume
bindsym $alt + F12 exec amixer -q set Master 5%+
bindsym $alt + F11  exec amixer -q set Master 5%-
bindsym $alt + F10 exec amixer set Master toggle

# music control
bindsym F10 exec ~/bin/spotify_remote.sh next
bindsym F9 exec ~/bin/spotify_remote.sh previous
bindsym F12 exec ~/bin/spotify_remote.sh play
bindsym F11 exec ~/bin/spotify_remote.sh pause 

# kill focused window
bindsym $super+c kill
bindsym $alt+F4 kill

# change focus
bindsym $super+Left focus left
bindsym $super+Down focus down
bindsym $super+Up focus up
bindsym $super+Right focus right

# move focused window
bindsym $super+Shift+Left move left
bindsym $super+Shift+Down move down
bindsym $super+Shift+Up move up
bindsym $super+Shift+Right move right

# split in horizontal orientation
bindsym $super+h split h

# split in vertical orientation
bindsym $super+v split v

# enter fullscreen mode for the focused container
bindsym $super+f fullscreen toggle

# change container layout split
bindsym $super+s layout toggle split

# toggle tiling / floating
bindsym $super+space floating toggle

# change focus between tiling / floating windows
bindsym $super+Shift+space focus mode_toggle

# switch to workspace
bindsym $alt+Control+Right workspace next
bindsym $alt+Control+Left workspace prev
bindsym $super+1 workspace 1
bindsym $super+2 workspace 2
bindsym $super+3 workspace 3
bindsym $super+4 workspace 4
bindsym $super+5 workspace 5
bindsym $super+6 workspace 6
bindsym $super+7 workspace 7

# move focused container to workspace
bindsym $super+Shift+1 move container to workspace 1
bindsym $super+Shift+2 move container to workspace 2
bindsym $super+Shift+3 move container to workspace 3
bindsym $super+Shift+4 move container to workspace 4
bindsym $super+Shift+5 move container to workspace 5
bindsym $super+Shift+6 move container to workspace 6
bindsym $super+Shift+7 move container to workspace 7

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $super+Shift+r restart

# exit i3
bindsym $super+q exec "i3-nagbar -t warning -m 'Really, exit?' -b 'Yes' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym Left resize shrink width 5 px or 5 ppt
        bindsym Down resize grow height 5 px or 5 ppt
        bindsym Up resize shrink height 5 px or 5 ppt
        bindsym Right resize grow width 5 px or 5 ppt
        bindsym Return mode "default"
}
bindsym $super+r mode "resize"

# panel
bar {
	colors {
        background #2f343f
        statusline #2f343f
        separator #4b5262

		# colour of border, background, and text
        focused_workspace	#2f343f	#bf616a	#d8dee8
        active_workspace	#2f343f	#2f343f	#d8dee8
        inactive_workspace	#2f343f	#2f343f	#d8dee8
        urgent_workspace	#2f343f	#ebcb8b	#2f343f
    }
        status_command py3status -c /home/bo/.config/i3status/config
}
# window rules, you can find the window class using xprop
# Toggle this to a greater value if you want borders. Borders are for whiners tho
for_window [class=".*"] border pixel 0

# Open terminal in window 1 always, if URxvt is used 
assign [class=emacs] 1
assign [class=brave-browser-nightly] 2 
assign [class=spotify] 5
assign [class=URxvt] 3
# Place holders for syntax for window rules
#assign [class=Firefox|Transmission-gtk] 2

# colour of border, background, text, indicator, and child_border
client.focused			#bf616a #2f343f #d8dee8 #bf616a #d8dee8
client.focused_inactive	#2f343f #2f343f #d8dee8 #2f343f #2f343f
client.unfocused		#2f343f #2f343f #d8dee8 #2f343f #2f343f
client.urgent			#2f343f #2f343f #d8dee8 #2f343f #2f343f
client.placeholder		#2f343f #2f343f #d8dee8 #2f343f #2f343f
client.background		#2f343f


### i3-gaps stuff ###

# Necessary for i3-gaps to work properly (pixel can be any value)
for_window [class="^.*"] border pixel 3

# Smart Gaps
smart_gaps on

# Smart Borders
smart_borders on

# Set inner/outer gaps
gaps inner 14
gaps outer 0

# Gaps mode
set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindsym $Super+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 15
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

