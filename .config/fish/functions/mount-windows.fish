#!/usr/bin/fish

function mount-sans
	read -ls line
	set passwd $line
	sudo mount //gc-san01/GC-Document /mnt/windows/GC-doc -o username=bomi@globalconnect.dk,password=$passwd
	sudo mount //gc-san01/Drive_U /mnt/windows/GC-U -o username=bomi@globalconnect.dk,password=$passwd
	sudo mount //gc-san01/ /mnt/windows/gc-sans01 -o username=bomi@globalconnect.dk,password=$passwd
end

function umount-sans
	read -ls line
	set passwd $line
	sudo umount /mnt/windows/*
end
