#!/usr/bin/fish

function fish_prompt
    echo -n ""
    set -l textcol  white
    set -l bgcol    blue
    set -l arrowcol green
    set_color $textcol -b $bgcol
    echo -n " "(basename $PWD)" "
    set_color $arrowcol -b normal
    echo -n "⮀ "
end
