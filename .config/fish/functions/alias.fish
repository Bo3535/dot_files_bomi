#!/usr/bin/fish

function changesh
    sudo update-alternatives --config x-terminal-emulator $argv
end

function firefox
    /usr/bin/firefox $argv
end

function tdc
    w3m redirect.tdchotspot.dk $argv
end
   
function stop_docker
    sudo systemctl stop docker and sudo systemctl stop libvirtd and sudo systemctl stop containerd
end

function go
    /usr/local/go/bin/go $argv
end

function nim
    /home/bo/.nimble/bin/nim $argv
end
