#!/usr/bin/fish
source ~/.config/fish/functions/alias.fish
#source ~/.config/fish/functions/prompt.fish
source ~/.config/fish/functions/keybinds.fish
source ~/.config/fish/functions/mount-windows.fish

set -gx PATH /home/bo/.local/bin $PATH

